package cryptohelper

import (
	"crypto/md5"
	"encoding/hex"
	"io"
	"os"
)

func MD5Encrypt(s string) string {
	h := md5.New()
	h.Write([]byte(s))
	return hex.EncodeToString(h.Sum(nil))
}

func MD5FileEncrypt(filepath string) (encrypt string, err error) {
	file, err := os.Open(filepath)
	if err != nil {
		return
	}
	defer file.Close()

	h := md5.New()
	if _, err = io.Copy(h, file); err != nil {
		return
	}
	return hex.EncodeToString(h.Sum(nil)), nil
}
