package cryptohelper

import (
	"crypto/sha256"
	"encoding/hex"
	"io"
	"os"
)

func SHA256Encrypt(s string) string {
	h := sha256.Sum256([]byte(s))
	return hex.EncodeToString(h[:])
}

func SHA256FileEncrypt(filepath string) (encrypt string, err error) {
	file, err := os.Open(filepath)
	if err != nil {
		return
	}
	defer file.Close()
	h := sha256.New()
	if _, err = io.Copy(h, file); err != nil {
		return
	}
	return hex.EncodeToString(h.Sum(nil)), nil
}
