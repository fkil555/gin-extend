package cryptohelper

import "testing"

func TestSHA256Encrypt(t *testing.T) {
	encrypt := SHA256Encrypt("123456")
	if encrypt != "8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92" {
		t.Fatal("SHA256Encrypt encode error")
	}
}