package cryptohelper

import "testing"

func TestSHA1Encrypt(t *testing.T) {
	encrypt := SHA1Encrypt("123456")
	if encrypt != "7c4a8d09ca3762af61e59520943dc26494f8941b" {
		t.Fatal("SHA1Encrypt encode error")
	}
}
