package cryptohelper

import "testing"

func TestMD5Encrypt(t *testing.T) {
	encrypt := MD5Encrypt("123456")
	if encrypt != "e10adc3949ba59abbe56e057f20f883e" {
		t.Fatal("MD5Encrypt encode error")
	}
}
