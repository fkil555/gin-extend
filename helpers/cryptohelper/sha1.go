package cryptohelper

import (
	"crypto/sha1"
	"encoding/hex"
	"io"
	"os"
)

func SHA1Encrypt(s string) string {
	h := sha1.New()
	h.Write([]byte(s))
	bs := h.Sum(nil)
	return hex.EncodeToString(bs)
}

func SHA1FileEncrypt(filepath string) (encrypt string, err error) {
	file, err := os.Open(filepath)
	if err != nil {
		return
	}
	defer file.Close()
	h := sha1.New()
	if _, err = io.Copy(h, file); err != nil {
		return
	}
	bs := h.Sum(nil)
	return hex.EncodeToString(bs), nil
}
