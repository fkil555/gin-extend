package versionhelper

import (
	"testing"
)

func TestVersionCompare(t *testing.T) {
	var ok bool
	var err error
	if ok, err = VersionCompare("1.2.3.4", "1.2.3.4", "=="); err != nil || !ok {
		t.Fatal()
	}
	if ok, err = VersionCompare("1.2.3.5", "1.2.4.4", "<"); err != nil || !ok {
		t.Fatal()
	}
	if ok, err = VersionCompare("1.2", "1.2.4.4", "<"); err != nil || !ok {
		t.Fatal()
	}
	if ok, err = VersionCompare("1.2.3.5", "1.2.4.4", "<="); err != nil || !ok {
		t.Fatal()
	}
	if ok, err = VersionCompare("1.2", "1.2.4.4", "<="); err != nil || !ok {
		t.Fatal()
	}
	if ok, err = VersionCompare("1.2.3.4", "1.2.2.4", ">"); err != nil || !ok {
		t.Fatal()
	}
	if ok, err = VersionCompare("1.2.3.4", "1.2.2.4", ">="); err != nil || !ok {
		t.Fatal()
	}
	if ok, err = VersionCompare("1.2.3.4", "1.2.3.4", ">="); err != nil || !ok {
		t.Fatal()
	}
	if _, err = VersionCompare("1.2.3.4", "1.2.3.4", "==="); err == nil {
		t.Fatal()
	}
}
