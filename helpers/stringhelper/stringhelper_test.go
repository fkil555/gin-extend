package stringhelper

import (
	"testing"
)

func TestStrStr(t *testing.T) {
	var rs string
	var exist bool
	str := "hello world!"
	_, exist = StrStr(str, "lol", true)
	if exist {
		t.Fatal("TestStrStr assert error")
	}
	rs, exist = StrStr(str, "lo", true);
	if !exist || rs != "hel" {
		t.Fatal(rs)
	}
	rs, exist = StrStr(str, "lo", false);
	if  !exist || rs != "lo world!" {
		t.Fatal(rs)
	}
}

func TestStrContains(t *testing.T) {
	strs := []string{"abc", "def", "ghi"}
	exist := StrContains(strs, "abc")
	if !exist {
		t.Fatal()
	}
	exist = StrContains(strs, "bcd")
	if exist {
		t.Fatal()
	}
}

func TestIsNum(t *testing.T) {
	if !IsNum("123") {
		t.Fatal()
	}
	if IsNum("abc") {
		t.Fatal()
	}
}

func TestIsEmpty(t *testing.T) {
	rs := IsEmpty("abc", "def")
	if rs != "abc" {
		t.Fatal()
	}
	rs = IsEmpty("", "def")
	if rs != "def" {
		t.Fatal()
	}
}