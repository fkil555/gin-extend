package stringhelper

import (
	"testing"
)

func TestTrim(t *testing.T) {
    str := "this is a string"
    str1 := " this is a string "
    str2 := "this is a string\n"
    str3 := "\vthis is a string "
    str4 := "this is a string\t"
    str5 := " this is a string\r\n"

    str6 := " this is a string"

    if str != Trim(str1) {
        t.Fatalf("测试" + str1 + "失败")
    }
    if str != Trim(str2) {
        t.Fatalf("测试" + str1 + "失败")
    }
    if str != Trim(str3) {
        t.Fatalf("测试" + str1 + "失败")
    }
    if str != Trim(str4) {
        t.Fatalf("测试" + str1 + "失败")
    }
    if str != Trim(str5) {
        t.Fatalf("测试" + str1 + "失败")
    }

    // 这里比对的是字符，不是字符串
    if Trim(str6, "string") != " this is a " {
        t.Fatalf("测试" + "this is a" + "失败")
    }
}
func TestTrimLeft(t *testing.T) {
    str := "this is a string"
    str1 := " this is a string"
    str2 := "\nthis is a string"
    str3 := "\tthis is a string"
    str4 := "\vthis is a string"
    str5 := "this is a string"


    if str != TrimLeft(str1) {
        t.Fatalf("测试" + str1 + "失败")
    }
    if str != TrimLeft(str2) {
        t.Fatalf("测试" + str1 + "失败")
    }
    if str != TrimLeft(str3) {
        t.Fatalf("测试" + str1 + "失败")
    }
    if str != TrimLeft(str4) {
        t.Fatalf("测试" + str1 + "失败")
    }
    if Trim(str5, "this") != " is a string" {
        t.Fatalf("测试" + " is a string" + "失败")
    }
}
func TestTrimRight(t *testing.T) {
    str := "this is a string"
    str1 := "this is a string\n"
    str2 := "this is a string\r"
    str3 := "this is a string\r\n"
    str4 := "this is a string\t"
    str5 := "this is a string"


    if str != TrimRight(str1) {
        t.Fatalf("测试" + str1 + "失败")
    }
    if str != TrimRight(str2) {
        t.Fatalf("测试" + str1 + "失败")
    }
    if str != TrimRight(str3) {
        t.Fatalf("测试" + str1 + "失败")
    }
    if str != TrimRight(str4) {
        t.Fatalf("测试" + str1 + "失败")
    }
    if  Trim(str5, "string") != "his is a " {
        t.Fatalf("测试" + "his is a " + "失败")
    }
}
