package stringhelper

import (
	"strings"
)

var (
    emptyChar = string([]byte{
        '\t',
        '\n',
        '\r',
        '\f',
        '\v',
        '\b',
        ' ',
        0x00,
        0x85,
        0xA0,
    })
)

/**
* Trim 
*
* @param string
* @param ...string 按字符处理
*
* @return 
*/
func Trim(str string, characterMask ...string) string {
    return strings.Trim(str, getCharacterMask(characterMask))
}

func TrimLeft(str string, characterMask ...string) string {
    return strings.TrimLeft(str, getCharacterMask(characterMask))
}

func TrimRight(str string, characterMask ...string) string {
    return strings.TrimRight(str, getCharacterMask(characterMask))
}

func getCharacterMask(characterMask []string) string {
    if len(characterMask) == 0 {
        return emptyChar
    }
    return characterMask[0]
}
