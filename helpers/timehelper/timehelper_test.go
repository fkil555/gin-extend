package timehelper

import (
	"encoding/json"
	"fmt"
	"gitee.com/fkil555/gin-extend/constants"
	"testing"
	"time"
)

func TestGDatetimeFormat(t *testing.T) {
	type Account struct {
		Name         string    `json:"name"`
		Age          string    `json:"age"`
		CreationDate GDatetime `json:"creation_date"`
	}

	setDate := "2020-07-01 10:01:01"
	setTime, _ := time.Parse(constants.DATETIME_FORMAT, "2020-07-01 10:01:01")
	zSetTime := GDatetime(setTime)
	account := Account{
		Name:         "ge-go",
		Age:          "1",
		CreationDate: zSetTime,
	}

	if account.CreationDate.Format() != setDate {
		t.Fatalf("Format失败")
	}

	if account.CreationDate.Time().Unix() != setTime.Unix() {
		t.Fatalf("Time失败")
	}

	jsonInfo, _ := json.Marshal(account)
	account4 := Account{}
	err := json.Unmarshal(jsonInfo, &account4)
	if err != nil {
		t.Fatalf("反解失败%s", err.Error())
	}
	jStr, _ := json.Marshal(account4)
	fmt.Println("反解:", string(jStr))

	fmt.Println(string(jsonInfo))
	// 将json转化为map[string][string]
	mapInfo := make(map[string]string, 0)
	json.Unmarshal(jsonInfo, &mapInfo)
	if mapInfo["creation_date"] != setDate {
		t.Fatalf("json compare失败")
	}

}
