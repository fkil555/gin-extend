package timehelper

import (
	"fmt"
	"gitee.com/fkil555/gin-extend/constants"
	"time"
)

// datetime类型
type GDatetime time.Time

func (t GDatetime) MarshalJSON() ([]byte, error) {
	var stamp = fmt.Sprintf("\"%s\"", time.Time(t).Format(constants.DATETIME_FORMAT))
	return []byte(stamp), nil
}

func (t GDatetime) Time() (time.Time) {
	return time.Time(t)
}

func (t GDatetime) Format() (string) {
	return  time.Time(t).Format(constants.DATETIME_FORMAT)
}

func (t *GDatetime) UnmarshalText(data []byte) (err error) {
	var newT time.Time
	if newT, err = time.Parse(constants.DATETIME_FORMAT, string(data)); err != nil {
		return
	}
	*t = GDatetime(newT)
	return
}


