package arrayhelper

import (
	"reflect"
	"testing"
)

func TestArrayChunk(t *testing.T) {
	arr := []int{1, 2, 3, 4, 5, 6, 7, 8, 9}
	result := ArrayChunk(arr, 4)
	if len(result) != 3 {
		t.Fatalf("ArrayChunk 未分拆成3个子数组")
	}
	if !reflect.DeepEqual(result[0], []int{1, 2, 3, 4}) {
		t.Fatalf("ArrayChunk 第一组数据和预期不一致")
	}
	if !reflect.DeepEqual(result[1], []int{5, 6, 7, 8}) {
		t.Fatalf("ArrayChunk 第二组数据和预期不一致")
	}
	if !reflect.DeepEqual(result[2], []int{9}) {
		t.Fatalf("ArrayChunk 第三组数据和预期不一致")
	}
	arr2 := []int32{1, 2, 3, 4, 5, 6, 7, 8, 9}
	if len(ArrayChunk(arr2, 4)) != 3 {
		t.Fatalf("ArrayChunk int32类型未按预期拆分成3个子数组")
	}
	arr3 := []int64{1, 2, 3, 4, 5, 6, 7, 8, 9}
	if len(ArrayChunk(arr3, 4)) != 3 {
		t.Fatalf("ArrayChunk int64类型未按预期拆分成3个子数组")
	}
	arr4 := []string{"1", "2", "3", "4", "5", "6", "7", "8", "9"}
	if len(ArrayChunk(arr4, 4)) != 3 {
		t.Fatalf("ArrayChunk string类型未按预期拆分成3个子数组")
	}
}

func TestArrayUnique(t *testing.T) {
	arr := []int{1, 2, 3, 1, 2, 3, 1, 2, 3}
	if len(ArrayUnique(arr)) != 3 {
		t.Fatalf("int型排重失败")
	}

	arr2 := []int64{1, 2, 3, 1, 2, 3, 1, 2, 3}
	if len(ArrayUnique(arr2)) != 3 {
		t.Fatalf("int64型排重失败")
	}

	arr3 := []string{"a", "b", "c", "a", "b", "c", "a", "b", "c", "a", "b", "c"}
	if len(ArrayUnique(arr3)) != 3 {
		t.Fatalf("string型排重失败")
	}
}

func TestInArray(t *testing.T) {
	arr := []int{1, 2, 3, 4, 5}
	if !InArray(arr, 2) {
		t.Fatal("InArrayV2 预期元素在切片中")
	}
	if InArray(arr, 6) {
		t.Fatal("InArrayV2 预期元素不在切片中")
	}
	arr2 := []string{"abc", "def", "ghi"}
	if !InArray(arr2, "def") {
		t.Fatal("InArrayV2 预期元素在切片中")
	}
	if InArray(arr2, "jkl") {
		t.Fatal("InArrayV2 预期元素不在切片中")
	}
}
