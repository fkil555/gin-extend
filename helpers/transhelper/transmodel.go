package transhelper

// SpiderBaiduFanyi 基于百度的翻译爬虫
type SpiderBaiduFanyi struct {
	ApiKey    string `json:"api_key"`
	SecretKey string `json:"secret_key"`
}

type FaceFusionRequest struct {
	ImageTemplate struct {
		Image     string `json:"image"`
		ImageType string `json:"image_type"`
	} `json:"image_template"`
	ImageTarget struct {
		Image     string `json:"image"`
		ImageType string `json:"image_type"`
	} `json:"image_target"`
	Version string `json:"version"`
}

type FaceFusionResponse struct {
	Result struct {
		MergeImage string `json:"merge_image"`
	} `json:"result"`
	ErrorMsg  string `json:"error_msg"`
	ErrorCode int    `json:"error_code"`
}

type BaiduFanyiResponse struct {
	Result struct {
		From        string `json:"from"`
		TransResult []struct {
			Dst string `json:"dst"`
			Src string `json:"src"`
		} `json:"trans_result"`
		To string `json:"to"`
	} `json:"result"`
	ErrorMsg  string `json:"error_msg"`
	ErrorCode int    `json:"error_code"`
}
