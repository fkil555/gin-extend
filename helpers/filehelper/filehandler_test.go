package filehelper

import (
	"fmt"
	"testing"         

)
func TestFilePutContent(t *testing.T) {
    homePath, err := Home()
    if err != nil {
        t.Fatalf("获取家目录失败")
    }
    filePath := homePath + "/a/b/cd/e/" + "/test.txt"

    if ExtName(filePath) != "txt" {
        t.Fatalf("扩展类型获取失败")
    }

    if !IsDir(Dir(filePath)) {
        Mkdir(Dir(filePath))
    }
    if !IsDir(Dir(filePath)) {
        t.Fatalf("创建目录失败")
    }

    content := "this is a text string \nthis is another string"
    err = FilePutContent(filePath, content, false)
    if err != nil {
        t.Fatalf("写入数据失败")
    }

    readContent, err := FileGetContent(filePath)
    if err != nil {
        t.Fatalf("读取失败")
    }
    if readContent != content {
        t.Fatalf("读取数据不一致")
    }
    // 追加操作
    FilePutContent(filePath, "123", true)
    readContent, err = FileGetContent(filePath)
    if (readContent != content + "123" || err != nil) {
        t.Fatalf("追加内容错误")
    }

    // 删除文件
    Remove(filePath)
    if IsFile(filePath) {
        t.Fatalf("删除文件失败")
    }

    Remove(homePath + "/a")
    if IsDir(homePath + "/a") {
        t.Fatalf("删除目录失败")
    }
    files,_ := Glob(homePath + "/*", false)
    fmt.Println(files)
}
