package urlhelper

import "testing"

func TestReplaceURI(t *testing.T) {
	uri := "/user/123/post/456/"
	uriReplace := ReplaceURI(uri)
	if uriReplace != "user/{num}/post/{num}" {
		t.Fatal("ReplaceURI 处理异常")
	}
	t.Logf("ReplaceURI success")
}

func TestUrlBase(t *testing.T) {
	uri := "http://test.com/it/name.txt?aaa=1&bbb=2"
	res, err := UrlBase(uri)
	if err != nil {
		t.Fatal("UrlBase " + err.Error())
	}
	if res != "name.txt" {
		t.Fatal("UrlBase 处理异常")
	}
}
