package result

import "testing"

type Person struct {
	Name string
	Age  int
}

func TestError(t *testing.T) {
	res := Error(1, "this is a error")
	if res.ErrorCode != 1 {
		t.Fatal()
	}
	if res.ErrorMsg != "this is a error" {
		t.Fatal()
	}
}

func TestSuccess(t *testing.T) {
	res := Success()
	if res.ErrorCode != 0 {
		t.Fatal()
	}
	if res.ErrorMsg != "OK" {
		t.Fatal()
	}
}

func TestSuccessData(t *testing.T) {
	data := Person{
		Name: "lll",
		Age:  12,
	}
	res := SuccessData(data)
	if res.ErrorCode != 0 {
		t.Fatal()
	}
	if res.ErrorMsg != "OK" {
		t.Fatal()
	}
	if res.Data != data {
		t.Fatal()
	}
}
