package result

type Result struct {
	ErrorCode int    `json:"error_code"`
	ErrorMsg  string `json:"error_msg"`
}

type ResultData struct {
	ErrorCode int         `json:"error_code"`
	ErrorMsg  string      `json:"error_msg"`
	Data      interface{} `json:"data"`
}

func Error(errorCode int, errorMsg string) (res Result) {
	return Result{ErrorCode: errorCode, ErrorMsg: errorMsg}
}

func Success() (res Result) {
	return Result{ErrorMsg: "OK"}
}

func SuccessData(data interface{}) (res ResultData) {
	return ResultData{ErrorMsg: "OK", Data: data}
}
