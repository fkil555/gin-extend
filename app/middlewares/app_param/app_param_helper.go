package app_param

import (
	"context"
)

var defaultAppParam = &AppParam{}

// 内部业务请求之间透传的参数
type AppParam struct {
	From     string // 来源服务
	Version  string // 版本号
	Platform string // 平台: web android ios
}

func GetAppParam(ctx context.Context) (appParam *AppParam) {
	var ok bool
	appParam, ok = ctx.Value("appParam").(*AppParam)
	if !ok {
		appParam = defaultAppParam
	}
	return
}

// 获取来源
func GetFrom(ctx context.Context) (from string) {
	from = GetAppParam(ctx).From
	return
}

// 获取版本号
func GetVersion(ctx context.Context) (version string) {
	version = GetAppParam(ctx).Version
	return
}

// 获取平台
func GetPlatform(ctx context.Context) (platform string) {
	platform = GetAppParam(ctx).Platform
	return
}
