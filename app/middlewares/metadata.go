package middlewares

import (
	metadata2 "gitee.com/fkil555/gin-extend/app/middlewares/metadata"
	"gitee.com/fkil555/gin-extend/client/ghttp"
	"github.com/gin-gonic/gin"
)

func MetadataMiddleware() gin.HandlerFunc {
	return func(context *gin.Context) {
		metadata := &metadata2.MetaData{
			Gin: context,
		}
		metadata.TraceData.RequestURI = context.Request.RequestURI
		metadata.TraceData.Host = context.Request.Host
		metadata.TraceData.TraceID = context.GetHeader("X-Request-Id")
		metadata.TraceData.TraceStr = context.GetHeader("X-Trace-Str")
		context.Set(ghttp.CTX_TRACEDATA_KEY, &metadata.TraceData)
		context.Set("metadata", metadata)
	}
}

