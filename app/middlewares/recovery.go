package middlewares

import (
	"bytes"
	"fmt"
	"gitee.com/fkil555/gin-extend/conf"
	"net"
	"net/http"
	"os"
	"runtime"
	"strings"

	"github.com/gin-gonic/gin"
)

// 调用栈
func stack(skip int) string {
	buf := new(bytes.Buffer)

	for i := skip; ; i++ {
		pc, file, line, ok := runtime.Caller(i)
		if !ok {
			break
		}
		fmt.Fprintf(buf, "%s:%d (0x%x)\n", file, line, pc)
	}
	return buf.String()
}

// 崩溃日志中间件
func Recovery() gin.HandlerFunc {
	// 注册中间件
	return func(context *gin.Context) {
		defer func() {
			if err := recover(); err != nil {
				// 调试输出到命令行
				if conf.GEConf.Debug != 0 {
					fmt.Fprintln(os.Stderr, err, stack(3))
				}

				// 确认panic的原因是broken pipe/rst，说明写应答时客户端已经发来FIN，这个时候gin会panic，我们不要将这个panic报到cat error
				var brokenPipe bool
				if ne, ok := err.(*net.OpError); ok {
					if se, ok := ne.Err.(*os.SyscallError); ok {
						if strings.Contains(strings.ToLower(se.Error()), "broken pipe") || strings.Contains(strings.ToLower(se.Error()), "connection reset by peer") {
							brokenPipe = true
						}
					}
				}

				// 回写应答期间panic，客户端连接已断开，我们无法回复http status
				if brokenPipe {
					context.Error(err.(error)) // nolint: errcheck
					context.Abort()	// 抄自gin实现，但实际没什么用，因为我们是中间件回溯流程中abort，还是会回溯到最外层完成cat上报之类
				} else {
					// 强制应答code 500报错给客户端
					context.AbortWithStatus(http.StatusInternalServerError)
				}
			}
		}()

		// 调用下一个中间件
		context.Next()
	}
}
