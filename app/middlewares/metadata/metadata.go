package metadata

import (
	"gitee.com/fkil555/gin-extend/client/ghttp"

	"github.com/gin-gonic/gin"
)

// 请求链路元数据
type MetaData struct {
	Gin       *gin.Context
	TraceData ghttp.TraceData
}
