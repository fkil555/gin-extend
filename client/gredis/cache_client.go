package gredis

import (
	"context"
	"time"

	"github.com/go-redis/redis"
)

type CacheClient struct {
	cacheGroup *CACHEGroup     // 主从组
	ctx        context.Context // 上下文
	options    *CacheOptions   // 配置项
}

func newCacheClient(ctx context.Context, cacheGroup *CACHEGroup, optionFuncs ...OptionFunc) (cacheClient *CacheClient) {
	cacheClient = &CacheClient{
		cacheGroup: cacheGroup,
		ctx:        ctx,
		options:    generateOptions(optionFuncs),
	}
	return
}

func (cacheClient *CacheClient) clone() (clonedCacheClient *CacheClient) {
	clonedCacheClient = &CacheClient{}
	*clonedCacheClient = *cacheClient
	clonedCacheClient.options = clonedCacheClient.options.clone()
	return
}

func (cacheClient *CacheClient) WithForceMaster() (clonedCacheClient *CacheClient) {
	clonedCacheClient = cacheClient.clone()
	WithForceMaster()(clonedCacheClient.options)
	return
}

func (cacheClient *CacheClient) master() (client *redis.Client) {
	var err error
	if client, err = cacheClient.cacheGroup.ChooseConn(cacheClient.ctx, MASTER); err != nil {
		panic("")
	}
	return
}

func (cacheClient *CacheClient) slave() (client *redis.Client) {
	var err error
	accessType := SLAVE
	if cacheClient.options.forceMaster {
		accessType = MASTER
	}
	if client, err = cacheClient.cacheGroup.ChooseConn(cacheClient.ctx, accessType); err != nil {
		panic("")
	}
	return
}

func (cacheClient *CacheClient) PipelineMaster() redis.Pipeliner {
	client := cacheClient.master()
	return client.Pipeline()
}

func (cacheClient *CacheClient) PipelineSlave() redis.Pipeliner {
	client := cacheClient.slave()
	return client.Pipeline()
}

func (cacheClient *CacheClient) PipelinedMaster(fn func(pipeliner redis.Pipeliner) error) ([]redis.Cmder, error) {
	client := cacheClient.master()
	return client.Pipelined(fn)
}

func (cacheClient *CacheClient) PipelinedSlave(fn func(pipeliner redis.Pipeliner) error) ([]redis.Cmder, error) {
	client := cacheClient.slave()
	return client.Pipelined(fn)
}

func (cacheClient *CacheClient) TxPipelinedMaster(fn func(pipeliner redis.Pipeliner) error) ([]redis.Cmder, error) {
	client := cacheClient.master()
	return client.TxPipelined(fn)
}

func (cacheClient *CacheClient) TxPipelinedSlave(fn func(pipeliner redis.Pipeliner) error) ([]redis.Cmder, error) {
	client := cacheClient.slave()
	return client.TxPipelined(fn)
}

func (cacheClient *CacheClient) TxPipelineMaster() redis.Pipeliner {
	client := cacheClient.master()
	return client.TxPipeline()
}

func (cacheClient *CacheClient) TxPipelineSlave() redis.Pipeliner {
	client := cacheClient.slave()
	return client.TxPipeline()
}

func (cacheClient *CacheClient) Del(keys ...string) *redis.IntCmd {
	client := cacheClient.master()
	return client.Del(keys...)
}

func (cacheClient *CacheClient) Exists(keys ...string) *redis.IntCmd {
	client := cacheClient.slave()
	return client.Exists(keys...)
}

func (cacheClient *CacheClient) Expire(key string, expiration time.Duration) *redis.BoolCmd {
	client := cacheClient.master()
	return client.Expire(key, expiration)
}

func (cacheClient *CacheClient) PExpire(key string, expiration time.Duration) *redis.BoolCmd {
	client := cacheClient.master()
	return client.PExpire(key, expiration)
}

func (cacheClient *CacheClient) ExpireAt(key string, tm time.Time) *redis.BoolCmd {
	client := cacheClient.master()
	return client.ExpireAt(key, tm)
}

func (cacheClient *CacheClient) PExpireAt(key string, tm time.Time) *redis.BoolCmd {
	client := cacheClient.master()
	return client.PExpireAt(key, tm)
}

func (cacheClient *CacheClient) Rename(key, newkey string) *redis.StatusCmd {
	client := cacheClient.master()
	return client.Rename(key, newkey)
}

func (cacheClient *CacheClient) RenameNX(key, newkey string) *redis.BoolCmd {
	client := cacheClient.master()
	return client.RenameNX(key, newkey)
}

func (cacheClient *CacheClient) TTL(key string) *redis.DurationCmd {
	client := cacheClient.slave()
	return client.TTL(key)
}

func (cacheClient *CacheClient) PTTL(key string) *redis.DurationCmd {
	client := cacheClient.slave()
	return client.PTTL(key)
}

func (cacheClient *CacheClient) Scan(cursor uint64, match string, count int64) *redis.ScanCmd {
	client := cacheClient.slave()
	return client.Scan(cursor, match, count)
}

func (cacheClient *CacheClient) SScan(key string, cursor uint64, match string, count int64) *redis.ScanCmd {
	client := cacheClient.slave()
	return client.SScan(key, cursor, match, count)
}

func (cacheClient *CacheClient) HScan(key string, cursor uint64, match string, count int64) *redis.ScanCmd {
	client := cacheClient.slave()
	return client.HScan(key, cursor, match, count)
}

func (cacheClient *CacheClient) ZScan(key string, cursor uint64, match string, count int64) *redis.ScanCmd {
	client := cacheClient.slave()
	return client.ZScan(key, cursor, match, count)
}

func (cacheClient *CacheClient) Append(key, value string) *redis.IntCmd {
	client := cacheClient.master()
	return client.Append(key, value)
}
func (cacheClient *CacheClient) BitCount(key string, bitCount *redis.BitCount) *redis.IntCmd {
	client := cacheClient.slave()
	return client.BitCount(key, bitCount)
}

func (cacheClient *CacheClient) BitOpAnd(destKey string, keys ...string) *redis.IntCmd {
	client := cacheClient.master()
	return client.BitOpAnd(destKey, keys...)
}

func (cacheClient *CacheClient) BitOpOr(destKey string, keys ...string) *redis.IntCmd {
	client := cacheClient.master()
	return client.BitOpOr(destKey, keys...)
}
func (cacheClient *CacheClient) BitOpXor(destKey string, keys ...string) *redis.IntCmd {
	client := cacheClient.master()
	return client.BitOpXor(destKey, keys...)
}

func (cacheClient *CacheClient) BitOpNot(destKey string, key string) *redis.IntCmd {
	client := cacheClient.master()
	return client.BitOpNot(destKey, key)
}

func (cacheClient *CacheClient) BitPos(key string, bit int64, pos ...int64) *redis.IntCmd {
	client := cacheClient.slave()
	return client.BitPos(key, bit, pos...)
}

func (cacheClient *CacheClient) Decr(key string) *redis.IntCmd {
	client := cacheClient.master()
	return client.Decr(key)
}

func (cacheClient *CacheClient) DecrBy(key string, decrement int64) *redis.IntCmd {
	client := cacheClient.master()
	return client.DecrBy(key, decrement)
}

func (cacheClient *CacheClient) Get(key string) *redis.StringCmd {
	client := cacheClient.slave()
	return client.Get(key)
}

func (cacheClient *CacheClient) GetBit(key string, offset int64) *redis.IntCmd {
	client := cacheClient.slave()
	return client.GetBit(key, offset)
}

func (cacheClient *CacheClient) GetRange(key string, start, end int64) *redis.StringCmd {
	client := cacheClient.slave()
	return client.GetRange(key, start, end)
}

func (cacheClient *CacheClient) GetSet(key string, value interface{}) *redis.StringCmd {
	client := cacheClient.master()
	return client.GetSet(key, value)
}

func (cacheClient *CacheClient) Incr(key string) *redis.IntCmd {
	client := cacheClient.master()
	return client.Incr(key)
}

func (cacheClient *CacheClient) IncrBy(key string, value int64) *redis.IntCmd {
	client := cacheClient.master()
	return client.IncrBy(key, value)
}

func (cacheClient *CacheClient) IncrByFloat(key string, value float64) *redis.FloatCmd {
	client := cacheClient.master()
	return client.IncrByFloat(key, value)
}

func (cacheClient *CacheClient) MGet(keys ...string) *redis.SliceCmd {
	client := cacheClient.slave()
	return client.MGet(keys...)
}

func (cacheClient *CacheClient) MSet(pairs ...interface{}) *redis.StatusCmd {
	client := cacheClient.master()
	return client.MSet(pairs...)
}

func (cacheClient *CacheClient) MSetNX(pairs ...interface{}) *redis.BoolCmd {
	client := cacheClient.master()
	return client.MSetNX(pairs...)
}
func (cacheClient *CacheClient) Set(key string, value interface{}, expiration time.Duration) *redis.StatusCmd {
	client := cacheClient.master()
	return client.Set(key, value, expiration)
}
func (cacheClient *CacheClient) SetBit(key string, offset int64, value int) *redis.IntCmd {
	client := cacheClient.master()
	return client.SetBit(key, offset, value)
}
func (cacheClient *CacheClient) SetNX(key string, value interface{}, expiration time.Duration) *redis.BoolCmd {
	client := cacheClient.master()
	return client.SetNX(key, value, expiration)
}
func (cacheClient *CacheClient) SetXX(key string, value interface{}, expiration time.Duration) *redis.BoolCmd {
	client := cacheClient.master()
	return client.SetXX(key, value, expiration)
}
func (cacheClient *CacheClient) SetRange(key string, offset int64, value string) *redis.IntCmd {
	client := cacheClient.master()
	return client.SetRange(key, offset, value)
}
func (cacheClient *CacheClient) StrLen(key string) *redis.IntCmd {
	client := cacheClient.slave()
	return client.StrLen(key)
}
func (cacheClient *CacheClient) HDel(key string, fields ...string) *redis.IntCmd {
	client := cacheClient.master()
	return client.HDel(key, fields...)
}
func (cacheClient *CacheClient) HExists(key, field string) *redis.BoolCmd {
	client := cacheClient.slave()
	return client.HExists(key, field)
}
func (cacheClient *CacheClient) HGet(key, field string) *redis.StringCmd {
	client := cacheClient.slave()
	return client.HGet(key, field)
}
func (cacheClient *CacheClient) HGetAll(key string) *redis.StringStringMapCmd {
	client := cacheClient.slave()
	return client.HGetAll(key)
}
func (cacheClient *CacheClient) HIncrBy(key, field string, incr int64) *redis.IntCmd {
	client := cacheClient.master()
	return client.HIncrBy(key, field, incr)
}
func (cacheClient *CacheClient) HIncrByFloat(key, field string, incr float64) *redis.FloatCmd {
	client := cacheClient.master()
	return client.HIncrByFloat(key, field, incr)
}
func (cacheClient *CacheClient) HKeys(key string) *redis.StringSliceCmd {
	client := cacheClient.slave()
	return client.HKeys(key)
}
func (cacheClient *CacheClient) HLen(key string) *redis.IntCmd {
	client := cacheClient.slave()
	return client.HLen(key)
}
func (cacheClient *CacheClient) HMGet(key string, fields ...string) *redis.SliceCmd {
	client := cacheClient.slave()
	return client.HMGet(key, fields...)
}
func (cacheClient *CacheClient) HMSet(key string, fields map[string]interface{}) *redis.StatusCmd {
	client := cacheClient.master()
	return client.HMSet(key, fields)
}
func (cacheClient *CacheClient) HSet(key, field string, value interface{}) *redis.BoolCmd {
	client := cacheClient.master()
	return client.HSet(key, field, value)
}
func (cacheClient *CacheClient) HSetNX(key, field string, value interface{}) *redis.BoolCmd {
	client := cacheClient.master()
	return client.HSetNX(key, field, value)
}
func (cacheClient *CacheClient) HVals(key string) *redis.StringSliceCmd {
	client := cacheClient.slave()
	return client.HVals(key)
}
func (cacheClient *CacheClient) BLPop(timeout time.Duration, keys ...string) *redis.StringSliceCmd {
	client := cacheClient.master()
	return client.BLPop(timeout, keys...)
}
func (cacheClient *CacheClient) BRPop(timeout time.Duration, keys ...string) *redis.StringSliceCmd {
	client := cacheClient.master()
	return client.BRPop(timeout, keys...)
}
func (cacheClient *CacheClient) BRPopLPush(source, destination string, timeout time.Duration) *redis.StringCmd {
	client := cacheClient.master()
	return client.BRPopLPush(source, destination, timeout)
}
func (cacheClient *CacheClient) LIndex(key string, index int64) *redis.StringCmd {
	client := cacheClient.slave()
	return client.LIndex(key, index)
}
func (cacheClient *CacheClient) LInsert(key, op string, pivot, value interface{}) *redis.IntCmd {
	client := cacheClient.master()
	return client.LInsert(key, op, pivot, value)
}
func (cacheClient *CacheClient) LInsertBefore(key string, pivot, value interface{}) *redis.IntCmd {
	client := cacheClient.master()
	return client.LInsertBefore(key, pivot, value)
}
func (cacheClient *CacheClient) LInsertAfter(key string, pivot, value interface{}) *redis.IntCmd {
	client := cacheClient.master()
	return client.LInsertAfter(key, pivot, value)
}
func (cacheClient *CacheClient) LLen(key string) *redis.IntCmd {
	client := cacheClient.slave()
	return client.LLen(key)
}
func (cacheClient *CacheClient) LPop(key string) *redis.StringCmd {
	client := cacheClient.master()
	return client.LPop(key)
}
func (cacheClient *CacheClient) LPush(key string, values ...interface{}) *redis.IntCmd {
	client := cacheClient.master()
	return client.LPush(key, values...)
}
func (cacheClient *CacheClient) LPushX(key string, value interface{}) *redis.IntCmd {
	client := cacheClient.master()
	return client.LPushX(key, value)
}
func (cacheClient *CacheClient) LRange(key string, start, stop int64) *redis.StringSliceCmd {
	client := cacheClient.slave()
	return client.LRange(key, start, stop)
}
func (cacheClient *CacheClient) LRem(key string, count int64, value interface{}) *redis.IntCmd {
	client := cacheClient.master()
	return client.LRem(key, count, value)
}
func (cacheClient *CacheClient) LSet(key string, index int64, value interface{}) *redis.StatusCmd {
	client := cacheClient.master()
	return client.LSet(key, index, value)
}
func (cacheClient *CacheClient) LTrim(key string, start, stop int64) *redis.StatusCmd {
	client := cacheClient.master()
	return client.LTrim(key, start, stop)
}
func (cacheClient *CacheClient) RPop(key string) *redis.StringCmd {
	client := cacheClient.master()
	return client.RPop(key)
}
func (cacheClient *CacheClient) RPopLPush(source, destination string) *redis.StringCmd {
	client := cacheClient.master()
	return client.RPopLPush(source, destination)
}
func (cacheClient *CacheClient) RPush(key string, values ...interface{}) *redis.IntCmd {
	client := cacheClient.master()
	return client.RPush(key, values...)
}
func (cacheClient *CacheClient) RPushX(key string, value interface{}) *redis.IntCmd {
	client := cacheClient.master()
	return client.RPushX(key, value)
}
func (cacheClient *CacheClient) SAdd(key string, members ...interface{}) *redis.IntCmd {
	client := cacheClient.master()
	return client.SAdd(key, members...)
}
func (cacheClient *CacheClient) SCard(key string) *redis.IntCmd {
	client := cacheClient.slave()
	return client.SCard(key)
}
func (cacheClient *CacheClient) SDiff(keys ...string) *redis.StringSliceCmd {
	client := cacheClient.slave()
	return client.SDiff(keys...)
}
func (cacheClient *CacheClient) SDiffStore(destination string, keys ...string) *redis.IntCmd {
	client := cacheClient.master()
	return client.SDiffStore(destination, keys...)
}
func (cacheClient *CacheClient) SInter(keys ...string) *redis.StringSliceCmd {
	client := cacheClient.slave()
	return client.SInter(keys...)
}
func (cacheClient *CacheClient) SInterStore(destination string, keys ...string) *redis.IntCmd {
	client := cacheClient.master()
	return client.SInterStore(destination, keys...)
}
func (cacheClient *CacheClient) SIsMember(key string, member interface{}) *redis.BoolCmd {
	client := cacheClient.slave()
	return client.SIsMember(key, member)
}
func (cacheClient *CacheClient) SMembers(key string) *redis.StringSliceCmd {
	client := cacheClient.slave()
	return client.SMembers(key)
}
func (cacheClient *CacheClient) SMembersMap(key string) *redis.StringStructMapCmd {
	client := cacheClient.slave()
	return client.SMembersMap(key)
}
func (cacheClient *CacheClient) SMove(source, destination string, member interface{}) *redis.BoolCmd {
	client := cacheClient.master()
	return client.SMove(source, destination, member)
}
func (cacheClient *CacheClient) SPop(key string) *redis.StringCmd {
	client := cacheClient.master()
	return client.SPop(key)
}
func (cacheClient *CacheClient) SPopN(key string, count int64) *redis.StringSliceCmd {
	client := cacheClient.master()
	return client.SPopN(key, count)
}
func (cacheClient *CacheClient) SRandMember(key string) *redis.StringCmd {
	client := cacheClient.slave()
	return client.SRandMember(key)
}
func (cacheClient *CacheClient) SRandMemberN(key string, count int64) *redis.StringSliceCmd {
	client := cacheClient.slave()
	return client.SRandMemberN(key, count)
}
func (cacheClient *CacheClient) SRem(key string, members ...interface{}) *redis.IntCmd {
	client := cacheClient.master()
	return client.SRem(key, members...)
}
func (cacheClient *CacheClient) SUnion(keys ...string) *redis.StringSliceCmd {
	client := cacheClient.slave()
	return client.SUnion(keys...)
}
func (cacheClient *CacheClient) SUnionStore(destination string, keys ...string) *redis.IntCmd {
	client := cacheClient.master()
	return client.SUnionStore(destination, keys...)
}
func (cacheClient *CacheClient) ZAdd(key string, members ...redis.Z) *redis.IntCmd {
	client := cacheClient.master()
	return client.ZAdd(key, members...)
}
func (cacheClient *CacheClient) ZAddNX(key string, members ...redis.Z) *redis.IntCmd {
	client := cacheClient.master()
	return client.ZAddNX(key, members...)
}
func (cacheClient *CacheClient) ZAddXX(key string, members ...redis.Z) *redis.IntCmd {
	client := cacheClient.master()
	return client.ZAddXX(key, members...)
}
func (cacheClient *CacheClient) ZAddCh(key string, members ...redis.Z) *redis.IntCmd {
	client := cacheClient.master()
	return client.ZAddCh(key, members...)
}
func (cacheClient *CacheClient) ZAddNXCh(key string, members ...redis.Z) *redis.IntCmd {
	client := cacheClient.master()
	return client.ZAddNXCh(key, members...)
}
func (cacheClient *CacheClient) ZAddXXCh(key string, members ...redis.Z) *redis.IntCmd {
	client := cacheClient.master()
	return client.ZAddXXCh(key, members...)
}
func (cacheClient *CacheClient) ZIncr(key string, member redis.Z) *redis.FloatCmd {
	client := cacheClient.master()
	return client.ZIncr(key, member)
}
func (cacheClient *CacheClient) ZIncrNX(key string, member redis.Z) *redis.FloatCmd {
	client := cacheClient.master()
	return client.ZIncrNX(key, member)
}
func (cacheClient *CacheClient) ZIncrXX(key string, member redis.Z) *redis.FloatCmd {
	client := cacheClient.master()
	return client.ZIncrXX(key, member)
}
func (cacheClient *CacheClient) ZCard(key string) *redis.IntCmd {
	client := cacheClient.slave()
	return client.ZCard(key)
}
func (cacheClient *CacheClient) ZCount(key, min, max string) *redis.IntCmd {
	client := cacheClient.slave()
	return client.ZCount(key, min, max)
}
func (cacheClient *CacheClient) ZLexCount(key, min, max string) *redis.IntCmd {
	client := cacheClient.slave()
	return client.ZLexCount(key, min, max)
}
func (cacheClient *CacheClient) ZIncrBy(key string, increment float64, member string) *redis.FloatCmd {
	client := cacheClient.master()
	return client.ZIncrBy(key, increment, member)
}
func (cacheClient *CacheClient) ZInterStore(destination string, store redis.ZStore, keys ...string) *redis.IntCmd {
	client := cacheClient.master()
	return client.ZInterStore(destination, store, keys...)
}
func (cacheClient *CacheClient) ZPopMax(key string, count ...int64) *redis.ZSliceCmd {
	client := cacheClient.master()
	return client.ZPopMax(key, count...)
}
func (cacheClient *CacheClient) ZPopMin(key string, count ...int64) *redis.ZSliceCmd {
	client := cacheClient.master()
	return client.ZPopMin(key, count...)
}
func (cacheClient *CacheClient) ZRange(key string, start, stop int64) *redis.StringSliceCmd {
	client := cacheClient.slave()
	return client.ZRange(key, start, stop)
}
func (cacheClient *CacheClient) ZRangeWithScores(key string, start, stop int64) *redis.ZSliceCmd {
	client := cacheClient.slave()
	return client.ZRangeWithScores(key, start, stop)
}
func (cacheClient *CacheClient) ZRangeByScore(key string, opt redis.ZRangeBy) *redis.StringSliceCmd {
	client := cacheClient.slave()
	return client.ZRangeByScore(key, opt)
}
func (cacheClient *CacheClient) ZRangeByLex(key string, opt redis.ZRangeBy) *redis.StringSliceCmd {
	client := cacheClient.slave()
	return client.ZRangeByLex(key, opt)
}
func (cacheClient *CacheClient) ZRangeByScoreWithScores(key string, opt redis.ZRangeBy) *redis.ZSliceCmd {
	client := cacheClient.slave()
	return client.ZRangeByScoreWithScores(key, opt)
}
func (cacheClient *CacheClient) ZRank(key, member string) *redis.IntCmd {
	client := cacheClient.slave()
	return client.ZRank(key, member)
}
func (cacheClient *CacheClient) ZRem(key string, members ...interface{}) *redis.IntCmd {
	client := cacheClient.master()
	return client.ZRem(key, members...)
}
func (cacheClient *CacheClient) ZRemRangeByRank(key string, start, stop int64) *redis.IntCmd {
	client := cacheClient.master()
	return client.ZRemRangeByRank(key, start, stop)
}
func (cacheClient *CacheClient) ZRemRangeByScore(key, min, max string) *redis.IntCmd {
	client := cacheClient.master()
	return client.ZRemRangeByScore(key, min, max)
}
func (cacheClient *CacheClient) ZRemRangeByLex(key, min, max string) *redis.IntCmd {
	client := cacheClient.master()
	return client.ZRemRangeByLex(key, min, max)
}
func (cacheClient *CacheClient) ZRevRange(key string, start, stop int64) *redis.StringSliceCmd {
	client := cacheClient.slave()
	return client.ZRevRange(key, start, stop)
}
func (cacheClient *CacheClient) ZRevRangeWithScores(key string, start, stop int64) *redis.ZSliceCmd {
	client := cacheClient.slave()
	return client.ZRevRangeWithScores(key, start, stop)
}
func (cacheClient *CacheClient) ZRevRangeByScore(key string, opt redis.ZRangeBy) *redis.StringSliceCmd {
	client := cacheClient.slave()
	return client.ZRevRangeByScore(key, opt)
}
func (cacheClient *CacheClient) ZRevRangeByLex(key string, opt redis.ZRangeBy) *redis.StringSliceCmd {
	client := cacheClient.slave()
	return client.ZRevRangeByLex(key, opt)
}
func (cacheClient *CacheClient) ZRevRangeByScoreWithScores(key string, opt redis.ZRangeBy) *redis.ZSliceCmd {
	client := cacheClient.slave()
	return client.ZRevRangeByScoreWithScores(key, opt)
}
func (cacheClient *CacheClient) ZRevRank(key, member string) *redis.IntCmd {
	client := cacheClient.slave()
	return client.ZRevRank(key, member)
}
func (cacheClient *CacheClient) ZScore(key, member string) *redis.FloatCmd {
	client := cacheClient.slave()
	return client.ZScore(key, member)
}
