package gredis

// 访问实例时的配置项
type CacheOptions struct {
	forceMaster bool
}

// 配置注入func
type OptionFunc func(options *CacheOptions)

// 生成配置
func generateOptions(optionFuncs []OptionFunc) (cacheOptions *CacheOptions) {
	cacheOptions = &CacheOptions{}
	if len(optionFuncs) > 0 {
		for _, f := range optionFuncs {
			f(cacheOptions)
		}
	}
	return
}

// 复制配置
func (options *CacheOptions) clone() (cloned *CacheOptions) {
	cloned = &CacheOptions{}
	*cloned = *options
	return
}

// 强制读主
func WithForceMaster() OptionFunc {
	return func(options *CacheOptions) {
		options.forceMaster = true
	}
}
