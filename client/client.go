package client

import (
	"sync"

	"gitee.com/fkil555/gin-extend/client/ghttp"
	"gitee.com/fkil555/gin-extend/client/gmysql"
	"gitee.com/fkil555/gin-extend/client/gpgsql"
	"gitee.com/fkil555/gin-extend/client/gredis"
	"gitee.com/fkil555/gin-extend/conf"
)

var clientOnce sync.Once

var closeOnce sync.Once

// // 初始化各种客户端
func InitClients() (err error) {
	clientOnce.Do(func() {
		// 拉起MYSQL
		if _, err = gmysql.NewMysql(conf.GEConf.Mysql); err != nil {
			return
		}
		if _, err = gpgsql.NewPgsql(conf.GEConf.Pgsql); err != nil {
			return
		}
		// 拉起REDIS
		if _, err = gredis.NewRedis(conf.GEConf.Redis); err != nil {
			return
		}
		// 初始化http-client参数
		if err = ghttp.InitHttpClient(conf.GEConf.HttpClientConfig); err != nil {
			return
		}
		return
	})
	return
}

func Close() (err error) {
	closeOnce.Do(func() {
		//
	})
	return
}
