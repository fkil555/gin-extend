package cron

// 封装Job，为了可以利用WrappedJob功能
type GEJob struct {
	f func()
}

func newJob(f func()) (job *GEJob) {
	job = &GEJob{
		f: f,
	}
	return
}

func (GEJob *GEJob) Run() {
	GEJob.f()
}
