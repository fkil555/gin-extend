package middlewares

import (
	cronContext "gitee.com/fkil555/gin-extend/cron/context"
)

// SetParams 设置参数, 如果参数为空，使用默认传参
func SetParams(str ...string) cronContext.HandleFunc {
	return func(ctx *cronContext.Context) {
		if len(ctx.Params) == 0 {
			ctx.Params = str
		}
		ctx.Next()
	}
}

