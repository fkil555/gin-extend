package middlewares

import (
	"context"
	"time"

	cronContext "gitee.com/fkil555/gin-extend/cron/context"
)

func WithTimeout(duration time.Duration) cronContext.HandleFunc {
	return func(ctx *cronContext.Context) {
		// 超时熔断
		timeoutCtx, cancelFunc := context.WithTimeout(ctx.Context, duration)
		defer cancelFunc()

		ctx.Context = timeoutCtx

		ctx.Next()
	}
}
