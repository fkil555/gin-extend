package cron

import (
	"sync"

	cronContext "gitee.com/fkil555/gin-extend/cron/context"
)

var r *router

type router struct {
	lock      sync.RWMutex
	routerMap map[string]*cronConf
}
type cronConf struct {
	name     string //cron别名
	execFunc []cronContext.HandleFunc
}

// 根据昵称拉取实际执行的函数
func getRouterByName(name string) (execFunc []cronContext.HandleFunc) {
	r.lock.RLock()
	defer r.lock.RUnlock()
	if router, ok := r.routerMap[name]; ok {
		return router.execFunc
	}
	return nil
}

func init() {
	r = &router{
		lock:      sync.RWMutex{},
		routerMap: make(map[string]*cronConf),
	}
}
