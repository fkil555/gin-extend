package conf

import (
	"flag"
	"fmt"
	"net"
	"os"
	"strings"

	"gitee.com/fkil555/gin-extend/client/ghttp"
	"gitee.com/fkil555/gin-extend/client/gmysql"
	"gitee.com/fkil555/gin-extend/client/gpgsql"
	"gitee.com/fkil555/gin-extend/client/gredis"

	"github.com/BurntSushi/toml"
)

var (
	confPath *string
	GEConf   *GEConfig
)

type MODE string

const MODE_APP MODE = "APP"
const MODE_CRON MODE = "CRON"
const MODE_GRPC MODE = "GRPC"

type GEConfig struct {
	Mode             MODE              // 模式 app, cron
	Domain           string            `toml:"domain"`      // 域名
	Debug            int               `toml:"debug"`       // 调试模式
	EnableCors       bool              `toml:"enable_cors"` //是否启用跨域
	Pprof            int               `toml:"pprof"`       // 性能采样功能
	Prometheus       int               `toml:"prometheus"`  // 普罗米修斯采集
	AppConfig        *AppConfig        `toml:"App"`         // App配置
	GrpcConfig       *GrpcConfig       `toml:"Grpc"`        // zgrpc-server配置
	CronConfig       *CronConfig       `toml:"Cron"`        // Cron配置
	TransConf        *TransConf        `toml:"TransConf"`   //翻译配置：百度
	Redis            *gredis.CACHEConf `toml:"Redis"`       //redis配置
	Mysql            *gmysql.DBConf    `toml:"Mysql"`       // mysql配置
	Pgsql            *gpgsql.DBConf    `toml:"Pgsql"`       // pgsql配置
	HttpClientConfig *ghttp.HttpConfig `toml:"HttpClient"`  //http-client配置
	ModuleName       string            `toml:"module_name"`
	CurrentIp        string
}

type TransConf struct {
	ApiKey    string `toml:"api_key"`
	SecretKey string `toml:"secret_key"`
}

// 默认配置
func defaultConf() *GEConfig {
	return &GEConfig{
		Debug:            0,
		Pprof:            0,
		Prometheus:       0,
		Domain:           "ge-go-undefined-domain",
		AppConfig:        DefaultAppConfig(),
		GrpcConfig:       DefaultGrpcConfig(),
		CronConfig:       DefaultCronConfig(),
		Redis:            nil,
		Mysql:            nil,
		Pgsql:            nil,
		HttpClientConfig: nil,
	}
}

// 初始化配置
func InitConf(mode MODE) (err error) {
	GEConf = defaultConf()
	if _, err = toml.DecodeFile(*confPath, GEConf); err != nil {
		return
	}
	// 如果容器实例有指定 INSTANCE IP 和 PORT 那么优先使用 实例指定的 IP 和 PORT
	ip := os.Getenv("INSTANCEIP")
	port := os.Getenv("PORT0")
	addr := fmt.Sprintf("%s:%s", ip, port)
	if len(addr) > 1 && len(strings.Split(addr, ":")) == 2 {
		// 说明获取到的环境变量不为空
		GEConf.AppConfig.Addr = addr
	}

	GEConf.Mode = mode
	if mode == MODE_APP && len(GEConf.AppConfig.Domain) > 0 {
		GEConf.Domain = GEConf.AppConfig.Domain
	}
	if mode == MODE_CRON && len(GEConf.CronConfig.Domain) > 0 {
		GEConf.Domain = GEConf.CronConfig.Domain
	}
	if mode == MODE_GRPC && len(GEConf.GrpcConfig.Domain) > 0 {
		GEConf.Domain = GEConf.GrpcConfig.Domain
	}
	if mode == MODE_CRON {
		loadCronContent()
	}
	initCurrentIp()
	return
}

// 初始化当前系统IP
func initCurrentIp() {
	var ip net.IP
	if ip == nil {
		GEConf.CurrentIp = "127.0.0.1"
		return
	}
	GEConf.CurrentIp = fmt.Sprintf("%d.%d.%d.%d", ip[12], ip[13], ip[14], ip[15])
}

func init() {
	confPath = flag.String("conf", "./app.toml", "框架配置文件路径")
}
