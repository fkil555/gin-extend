package conf

const (
	DEFAULT_READ_TIMEOUT   int = 5000
	DEFAULT_WRITE_TIMEOUT  int = 5000
	DEFAULT_HANDLE_TIMEOUT int = 5000
	DEFAULT_APP_WAIT           = 5000 // 默认退出等待时间
)

// APP配置
type AppConfig struct {
	Domain        string `toml:"domain"`        // 域名
	StaticDomain  string `toml:"static_domain"` //静态文件域名
	Addr          string `toml:"listen"`        // 监听地址
	ReadTimeout   int    `toml:"readTimeout"`   // 请求读超时
	HandleTimeout int    `toml:"handleTimeout"` // 请求处理超时
	WriteTimeout  int    `toml:"writeTimeout"`  // 请求写超时
	WaitGraceExit int    `toml:"waitGraceExit"` // 优雅退出等待时间

	// token配置
	TokenHeaderKey      string `toml:"token_header_key"`
	TokenHeaderAdminKey string `toml:"token_header_admin_key"`
	TokenAccessSecret   string `toml:"token_access_secret"` // token访问密钥
	TokenAccessExpire   int64  `toml:"token_access_expire"` // token过期时间
}

// 默认App配置
func DefaultAppConfig() (appConfig *AppConfig) {
	appConfig = &AppConfig{
		ReadTimeout:   DEFAULT_READ_TIMEOUT,
		HandleTimeout: DEFAULT_HANDLE_TIMEOUT,
		WriteTimeout:  DEFAULT_WRITE_TIMEOUT,
		WaitGraceExit: DEFAULT_APP_WAIT,
	}
	return
}
