package conf

const DEFAULT_GRPC_WAIT = 5000 // 统一默认值

// zgrpc-server配置
type GrpcConfig struct {
	Domain        string `toml:"domain"`        // 域名
	Addr          string `toml:"listen"`        // 监听地址
	ReadTimeout   int    `toml:"readTimeout"`   // 请求读超时
	HandleTimeout int    `toml:"handleTimeout"` // 请求处理超时
	WriteTimeout  int    `toml:"writeTimeout"`  // 请求写超时
	WaitGraceExit int    `toml:"waitGraceExit"` // 优雅退出等待时间
}

// 默认Grpc配置
func DefaultGrpcConfig() (grpcConfig *GrpcConfig) {
	grpcConfig = &GrpcConfig{
		ReadTimeout:   DEFAULT_GRPC_WAIT,
		HandleTimeout: DEFAULT_GRPC_WAIT,
		WriteTimeout:  DEFAULT_GRPC_WAIT,
		WaitGraceExit: DEFAULT_GRPC_WAIT,
	}
	return
}
