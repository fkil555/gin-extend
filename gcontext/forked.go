package gcontext

import (
	"context"
)

// 派生上下文, 保障框架的并发安全问题
type GEForkedContext struct {
	context.Context
}

// 派生协程使用GEContext之前必须调用该方法获取一个派生的context
func WithGEForkedContext(parent context.Context) (ctx *GEForkedContext) {
	ctx = &GEForkedContext{
		Context: parent,
	}
	return
}

func (ctx *GEForkedContext) Value(key interface{}) (value interface{}) {
	if strKey, ok := key.(string); ok && strKey == "FORK" { // 提供查看fork标记位的能力
		value = true
		return
	}
	value = ctx.Context.Value(key) // 否则从父亲取
	return
}
