package gcontext

import (
	"context"

	context2 "gitee.com/fkil555/gin-extend/cron/context"
	"github.com/gin-gonic/gin"
)

// 请求上下文
type GEContext struct {
	context.Context
	Gin        *gin.Context
	Cron       *context2.Context
}

// controller定义
type GEHandleFunc func(c *GEContext)
