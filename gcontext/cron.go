package gcontext

// GetCronParam 获取定时任务参数
func (ctx *GEContext) GetCronParam() (params []string) {
	return ctx.Cron.Params
}
